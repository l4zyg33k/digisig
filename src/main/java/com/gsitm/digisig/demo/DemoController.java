package com.gsitm.digisig.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class DemoController {

	@GetMapping("/demo/demo{number}")
	public String main(@PathVariable("number") Long number) {
		
		String url = String.format("demo/demo%s", number);
		return url;
	}
}
