package com.gsitm.digisig.repository;

import org.springframework.data.jpa.datatables.qrepository.QDataTablesRepository;

import com.gsitm.digisig.model.Division;

public interface DivisionRepository extends QDataTablesRepository<Division, Long> {

}
