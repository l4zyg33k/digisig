package com.gsitm.digisig.repository;

import org.springframework.data.jpa.datatables.qrepository.QDataTablesRepository;
import org.springframework.stereotype.Repository;

import com.gsitm.digisig.model.Content;

@Repository
public interface ContentRepository extends QDataTablesRepository<Content, Long> {

}
