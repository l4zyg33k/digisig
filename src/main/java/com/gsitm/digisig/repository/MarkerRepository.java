package com.gsitm.digisig.repository;

import org.springframework.data.jpa.datatables.qrepository.QDataTablesRepository;

import com.gsitm.digisig.model.Marker;

public interface MarkerRepository extends QDataTablesRepository<Marker, String> {

}
