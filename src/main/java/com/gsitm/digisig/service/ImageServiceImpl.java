package com.gsitm.digisig.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.imageio.ImageIO;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.moodysalem.phantomjs.wrapper.PhantomJS;
import com.moodysalem.phantomjs.wrapper.RenderException;
import com.moodysalem.phantomjs.wrapper.beans.BannerInfo;
import com.moodysalem.phantomjs.wrapper.beans.Margin;
import com.moodysalem.phantomjs.wrapper.beans.PaperSize;
import com.moodysalem.phantomjs.wrapper.beans.ViewportDimensions;
import com.moodysalem.phantomjs.wrapper.enums.RenderFormat;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ImageServiceImpl implements ImageService {

	@Autowired
	private ContentService service;

	@Override
	@Cacheable("getImageAsResource")
	public byte[] getImageAsResource(Long division, String marker) {
		try {
			/*
			SSLContext sslContext = SSLContextBuilder.create().loadTrustMaterial(new TrustSelfSignedStrategy()).build();
			HostnameVerifier allowAllHosts = new NoopHostnameVerifier();
			SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);
			*/
			
		    // Trust all certs
		    SSLContext sslcontext = buildSSLContext();

		    // Allow TLSv1 protocol only
		    @SuppressWarnings("deprecation")
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
		            sslcontext,
		            new String[] { "TLSv1" },
		            null,
		            SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		    
			CloseableHttpClient client = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
					.setSSLSocketFactory(sslsf).build();
			HttpGet request = new HttpGet(service.getUrl(division, marker));
			CloseableHttpResponse response = client.execute(request);

			InputStream html = response.getEntity().getContent();
			InputStream png = PhantomJS.render(null, html, PaperSize.A4, ViewportDimensions.VIEW_1280_1024, Margin.ZERO,
					BannerInfo.EMPTY, BannerInfo.EMPTY, RenderFormat.PNG, 10000L, 100L);
			BufferedImage originalImage = ImageIO.read(png);
			BufferedImage croppedImage = originalImage.getSubimage(0, 0, 1280, 1024);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(croppedImage, "png", baos);
			return baos.toByteArray();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException
				| RenderException e) {
			log.error(e.getMessage());
			return null;
		}
	}

	private static SSLContext buildSSLContext()
			throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
		SSLContext sslcontext = SSLContexts.custom().setSecureRandom(new SecureRandom())
				.loadTrustMaterial(null, new TrustStrategy() {

					@Override
					public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
						return true;
					}
				}).build();
		return sslcontext;
	}
}
