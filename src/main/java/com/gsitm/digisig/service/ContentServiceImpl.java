package com.gsitm.digisig.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.gsitm.digisig.model.Content;
import com.gsitm.digisig.model.QContent;
import com.gsitm.digisig.model.QDivision;
import com.gsitm.digisig.model.QMarker;
import com.gsitm.digisig.repository.ContentRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Service
public class ContentServiceImpl implements ContentService {

	@Autowired
	private ContentRepository repository;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Cacheable("getUrl")
	public String getUrl(Long division, String marker) {
		QDivision qDiv = QDivision.division;
		QMarker qMar = QMarker.marker;
		Optional<Content> content = repository.findOne(qDiv.id.eq(division).and(qMar.code.eq(marker)));
		return content.get().getUrl();
	}

	@Override
	@Cacheable("findByDivision")
	public List<Content> findByDivision(Long division) {
		QDivision qDiv = QDivision.division;
		return Lists.newArrayList(repository.findAll(qDiv.id.eq(division)));
	}

	@Override
	@Cacheable("getDashboardRow")
	public List<Content> getDashboardRow(Long row) {
		JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
		QContent content = QContent.content;

		Long offset = 0L;
		Long limit = 2L;

		if (row > 1L) {
			offset = 3L;
			limit = 5L;
		}

		return queryFactory.selectFrom(content).offset(offset).limit(limit).fetch();
	}
}
