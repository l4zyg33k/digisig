package com.gsitm.digisig.service;

import java.util.List;

import com.gsitm.digisig.model.Content;

public interface ContentService {

	public String getUrl(Long division, String marker);
	public List<Content> findByDivision(Long division);
	public List<Content> getDashboardRow(Long row);
}
