package com.gsitm.digisig.service;

public interface ImageService {

	public byte[] getImageAsResource(Long division, String marker);
}
