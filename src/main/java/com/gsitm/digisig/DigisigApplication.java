package com.gsitm.digisig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.datatables.qrepository.QDataTablesRepositoryFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * 
 * 
 *
 */
@SpringBootApplication
@EnableCaching
@EnableJpaRepositories(repositoryFactoryBeanClass = QDataTablesRepositoryFactoryBean.class)
public class DigisigApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigisigApplication.class, args);
	}
}
