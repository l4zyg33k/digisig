package com.gsitm.digisig.mobile.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.gsitm.digisig.model.Content;
import com.gsitm.digisig.service.ContentService;

/**
 * 
 * 
 *
 */
@Controller
public class MobileController {

	@Autowired
	private ContentService service;

	@GetMapping("/mobile/{division}")
	public String main(@PathVariable("division") Long division, Model model) {
		List<Content> contents = service.findByDivision(division);
		model.addAttribute("division", division);
		model.addAttribute("contents", contents);
		return "mobile/main";
	}
}
