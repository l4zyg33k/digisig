package com.gsitm.digisig.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.digisig.service.ImageService;

@RestController
public class ImageController {

	@Autowired
	private ImageService service;

	@GetMapping(value = "/img/{division}/{marker}.png", produces = MediaType.IMAGE_PNG_VALUE)
	public byte[] getImageAsResource(@PathVariable("division") Long division, @PathVariable("marker") String marker) {

		return service.getImageAsResource(division, marker);
	}
}
