package com.gsitm.digisig.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 
 * 
 *
 */
@Controller
public class DigisigController {

	@GetMapping("/")
	public String index() {
		return "redirect:/admin";
	}
	
	@GetMapping("/admin")
	public String admin() {
		return "index";
	}	

	@GetMapping("/admin/views/{view}.html")
	public String views(@PathVariable(value = "view") String view) {
		return String.format("views/%s", view);
	}

	@GetMapping("/admin/views/{category}/{view}.html")
	public String views(@PathVariable(value = "category") String category, @PathVariable(value = "view") String view) {
		return String.format("views/%s/%s", category, view);
	}
}
