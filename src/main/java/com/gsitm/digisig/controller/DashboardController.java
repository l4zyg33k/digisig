package com.gsitm.digisig.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gsitm.digisig.model.Content;
import com.gsitm.digisig.service.ContentService;

@RestController
public class DashboardController {

	@Autowired
	private ContentService service;

	@GetMapping("/api/dashboard/{row}")
	public List<Content> getDashboardRow(@PathVariable("row") Long row) {
		List<Content> result = service.getDashboardRow(row);
		return result;
	}
}
