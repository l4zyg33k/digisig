package com.gsitm.digisig.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ContentController {

	@GetMapping(value = "/contents/html/{filename}")
	public String getImageAsResource(@PathVariable("filename") String filename) {

		return String.format("contents/html/%s", filename);
	}
}
