package com.gsitm.digisig.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

/**
 * 
 * 
 *
 */
@Entity
@Data
public class Marker {

	@Id
	private String code;
	
	private String src;
}
