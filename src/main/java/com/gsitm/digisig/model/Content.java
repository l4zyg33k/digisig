package com.gsitm.digisig.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

/**
 * 
 * 
 *
 */
@Entity
@Table(uniqueConstraints = { @UniqueConstraint( name = "uk_content_1", columnNames = { "division_id", "marker_code" }) })
@Data
public class Content {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_content_1"))
	private Division division;

	@ManyToOne
	@JoinColumn(foreignKey = @ForeignKey(name = "fk_content_2"))
	private Marker marker;

	private String url;

	private String title;
}
