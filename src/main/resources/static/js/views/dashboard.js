$(function() {
	'use strict';
	
	var template = $('#card-block-row').html();
	var row1Temp = Handlebars.compile(template);
	var row2Temp = Handlebars.compile(template);
	
	$.get('/api/dashboard/1', function(data) {
		var row1Html = row1Temp(data);
		console.log(row1Html);
		$('#card-block-rows').append(row1Html);
	});
	
	$.get('/api/dashboard/2', function(data) {
		var row2Html = row2Temp(data);
		$('#card-block-rows').append(row2Html);
	});	
});