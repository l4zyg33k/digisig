CREATE TABLE content
  (
     id          BIGINT NOT NULL,
     title       VARCHAR(255),
     url         VARCHAR(255),
     division_id BIGINT,
     marker_code VARCHAR(255),
     PRIMARY KEY (id)
  );

CREATE TABLE division
  (
     id   BIGINT NOT NULL,
     name VARCHAR(255),
     PRIMARY KEY (id)
  );

CREATE TABLE marker
  (
     code VARCHAR(255) NOT NULL,
     src  VARCHAR(255),
     PRIMARY KEY (code)
  );

ALTER TABLE content
  ADD CONSTRAINT uk_content_1 UNIQUE (division_id, marker_code);

ALTER TABLE content
  ADD CONSTRAINT fk_content_1 FOREIGN KEY (division_id) REFERENCES division(id);

ALTER TABLE content
  ADD CONSTRAINT fk_content_2 FOREIGN KEY (marker_code) REFERENCES marker(code);