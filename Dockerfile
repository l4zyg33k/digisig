FROM ubuntu:17.10

VOLUME /tmp

RUN apt-get update && \
    apt-get install -y openjdk-8-jdk fontconfig language-pack-ko fonts-unfonts-core fonts-unfonts-extra fonts-nanum fonts-nanum-extra build-essential chrpath libssl-dev libxft-dev libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev && \
	apt-get clean && \
	locale-gen ko_KR.UTF-8 && \
	fc-cache -vf

ENV LANG ko_KR.UTF-8 LANGUAGE ko_KR.UTF-8 LC_ALL ko_KR.UTF-8
	
ADD ./target/digisig-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]